<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                if (0 === strpos($pathinfo, '/_profiler/i')) {
                    // _profiler_info
                    if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                    }

                    // _profiler_import
                    if ($pathinfo === '/_profiler/import') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:importAction',  '_route' => '_profiler_import',);
                    }

                }

                // _profiler_export
                if (0 === strpos($pathinfo, '/_profiler/export') && preg_match('#^/_profiler/export/(?P<token>[^/\\.]++)\\.txt$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_export')), array (  '_controller' => 'web_profiler.controller.profiler:exportAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/hotel')) {
            // uts_hotel_homepage
            if (rtrim($pathinfo, '/') === '/hotel') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'uts_hotel_homepage');
                }

                return array (  '_controller' => 'Uts\\HotelBundle\\Controller\\DefaultController::indexAction',  '_route' => 'uts_hotel_homepage',);
            }

            // uts_hotel_search_process
            if ($pathinfo === '/hotel/process') {
                return array (  '_controller' => 'Uts\\HotelBundle\\Controller\\DefaultController::processAction',  '_route' => 'uts_hotel_search_process',);
            }

            // uts_hotel_search_results
            if (0 === strpos($pathinfo, '/hotel/results') && preg_match('#^/hotel/results/(?P<searchId>[^/]++)(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'uts_hotel_search_results')), array (  '_controller' => 'Uts\\HotelBundle\\Controller\\DefaultController::resultsAction',  'page' => 1,));
            }

            if (0 === strpos($pathinfo, '/hotel/specialoffer')) {
                // specialoffer
                if (rtrim($pathinfo, '/') === '/hotel/specialoffer') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'specialoffer');
                    }

                    return array (  '_controller' => 'Uts\\HotelBundle\\Controller\\SpecialOfferController::indexAction',  '_route' => 'specialoffer',);
                }

                // specialoffer_show
                if (preg_match('#^/hotel/specialoffer/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'specialoffer_show')), array (  '_controller' => 'Uts\\HotelBundle\\Controller\\SpecialOfferController::showAction',));
                }

                // specialoffer_new
                if ($pathinfo === '/hotel/specialoffer/new') {
                    return array (  '_controller' => 'Uts\\HotelBundle\\Controller\\SpecialOfferController::newAction',  '_route' => 'specialoffer_new',);
                }

                // specialoffer_create
                if ($pathinfo === '/hotel/specialoffer/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_specialoffer_create;
                    }

                    return array (  '_controller' => 'Uts\\HotelBundle\\Controller\\SpecialOfferController::createAction',  '_route' => 'specialoffer_create',);
                }
                not_specialoffer_create:

                // specialoffer_edit
                if (preg_match('#^/hotel/specialoffer/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'specialoffer_edit')), array (  '_controller' => 'Uts\\HotelBundle\\Controller\\SpecialOfferController::editAction',));
                }

                // specialoffer_update
                if (preg_match('#^/hotel/specialoffer/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_specialoffer_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'specialoffer_update')), array (  '_controller' => 'Uts\\HotelBundle\\Controller\\SpecialOfferController::updateAction',));
                }
                not_specialoffer_update:

                // specialoffer_delete
                if (preg_match('#^/hotel/specialoffer/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_specialoffer_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'specialoffer_delete')), array (  '_controller' => 'Uts\\HotelBundle\\Controller\\SpecialOfferController::deleteAction',));
                }
                not_specialoffer_delete:

            }

        }

        // uts_content_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'uts_content_homepage');
            }

            return array (  '_controller' => 'Uts\\ContentBundle\\Controller\\DefaultController::indexAction',  '_route' => 'uts_content_homepage',);
        }

        // uts_content_prepare
        if ($pathinfo === '/instruction') {
            return array (  '_controller' => 'Uts\\ContentBundle\\Controller\\DefaultController::prepareAction',  '_route' => 'uts_content_prepare',);
        }

        // uts_content_task
        if ($pathinfo === '/task') {
            return array (  '_controller' => 'Uts\\ContentBundle\\Controller\\DefaultController::taskAction',  '_route' => 'uts_content_task',);
        }

        if (0 === strpos($pathinfo, '/shtumi_')) {
            if (0 === strpos($pathinfo, '/shtumi_ajax')) {
                // shtumi_ajaxautocomplete
                if ($pathinfo === '/shtumi_ajaxautocomplete') {
                    return array (  '_controller' => 'Shtumi\\UsefulBundle\\Controller\\AjaxAutocompleteJSONController::getJSONAction',  '_route' => 'shtumi_ajaxautocomplete',);
                }

                // shtumi_ajaxfileupload
                if ($pathinfo === '/shtumi_ajaxfileupload') {
                    return array (  '_controller' => 'Shtumi\\UsefulBundle\\Controller\\AjaxFileController::uploadAction',  '_route' => 'shtumi_ajaxfileupload',);
                }

            }

            if (0 === strpos($pathinfo, '/shtumi_dependent_filtered_')) {
                // shtumi_dependent_filtered_entity
                if ($pathinfo === '/shtumi_dependent_filtered_entity') {
                    return array (  '_controller' => 'ShtumiUsefulBundle:DependentFilteredEntity:getOptions',  '_route' => 'shtumi_dependent_filtered_entity',);
                }

                // shtumi_dependent_filtered_select2
                if ($pathinfo === '/shtumi_dependent_filtered_select2') {
                    return array (  '_controller' => 'Shtumi\\UsefulBundle\\Controller\\DependentFilteredEntityController::getJsonAction',  '_route' => 'shtumi_dependent_filtered_select2',);
                }

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
