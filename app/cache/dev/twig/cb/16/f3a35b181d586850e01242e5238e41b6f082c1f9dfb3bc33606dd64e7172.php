<?php

/* UtsHotelBundle:Form:search.html.twig */
class __TwigTemplate_cb16f3a35b181d586850e01242e5238e41b6f082c1f9dfb3bc33606dd64e7172 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'form' => array($this, 'block_form'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('form', $context, $blocks);
    }

    public function block_form($context, array $blocks = array())
    {
        // line 2
        echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/utshotel/css/datepicker.css"), "html", null, true);
        echo "\" />
    <script type=\"text/javascript\" src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/utshotel/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>

    ";
        // line 5
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('routing')->getPath("uts_hotel_search_process"), "role" => "form"));
        echo "
        ";
        // line 6
        if ((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) {
            // line 7
            echo "            <div class=\"row\">
            ";
            // line 8
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 9
                echo "                <div class=\"col-lg-12 alert alert-danger\">
                    <p>
                        ";
                // line 11
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "message"), "html", null, true);
                echo "
                    </p>
                </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 15
            echo "            </div>
        ";
        }
        // line 17
        echo "        <div class=\"row\">
            <div class=\"form-group col-lg-3";
        // line 18
        if ((!$this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "city"), "vars"), "valid"))) {
            echo " has-error";
        }
        echo "\">
                ";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "city"), 'label');
        echo "
                ";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "city"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
            <div class=\"form-group col-lg-2";
        // line 22
        if ((!$this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "checkIn"), "vars"), "valid"))) {
            echo " has-error";
        }
        echo "\">
                ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "checkIn"), 'label');
        echo "
                ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "checkIn"), 'widget', array("attr" => array("class" => "form-control datepicker")));
        echo "
            </div>
            <div class=\"form-group col-lg-2";
        // line 26
        if ((!$this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "checkOut"), "vars"), "valid"))) {
            echo " has-error";
        }
        echo "\">
                ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "checkOut"), 'label');
        echo "
                ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "checkOut"), 'widget', array("attr" => array("class" => "form-control datepicker")));
        echo "
            </div>
            <div class=\"form-group col-lg-1";
        // line 30
        if ((!$this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adults"), "vars"), "valid"))) {
            echo " has-error";
        }
        echo "\">
                ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adults"), 'label');
        echo "
                ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adults"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
           <div class=\"form-group col-lg-1\">
                ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "search"), 'widget', array("attr" => array("class" => "btn btn-primary btn-lg")));
        echo "
            </div>
        </div>
    ";
        // line 38
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    <script type=\"text/javascript\">
        \$(document).ready(
                function(){
                    \$('.datepicker').datepicker(
                            {
                                format:'dd.mm.yyyy',
                                onRender: function(date) {
                                    var now = new Date();
                                    return date.valueOf() < now.valueOf() ? 'disabled' : '';
                                }
                            }
                    );
                }
        );
    </script>
";
    }

    public function getTemplateName()
    {
        return "UtsHotelBundle:Form:search.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  137 => 38,  131 => 35,  125 => 32,  121 => 31,  115 => 30,  110 => 28,  106 => 27,  100 => 26,  95 => 24,  91 => 23,  85 => 22,  80 => 20,  76 => 19,  70 => 18,  67 => 17,  63 => 15,  53 => 11,  49 => 9,  45 => 8,  42 => 7,  40 => 6,  36 => 5,  31 => 3,  26 => 2,  20 => 1,);
    }
}
