<?php

/* base.html.twig */
class __TwigTemplate_7560b14128e8c0aca13290e1c7e201dac6f49cf101c1406f6c0a2a00765d5b82 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'homepage_active' => array($this, 'block_homepage_active'),
            'prepare_active' => array($this, 'block_prepare_active'),
            'task_active' => array($this, 'block_task_active'),
            'test_active' => array($this, 'block_test_active'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 13
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
</head>
<body>
    <div class=\"container\">
        <div class=\"navbar navbar-default\">
            <div class=\"navbar-header\">
                <div class=\"navbar-brand\">ЮТС тест</div>
            </div>
            <div class=\"container-fluid\">
                <div class=\"collapse navbar-collapse\">
                    <ul class=\"nav navbar-nav\">
                        <li ";
        // line 24
        $this->displayBlock('homepage_active', $context, $blocks);
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("uts_content_homepage");
        echo "\">Для начала</a></li>
                        <li ";
        // line 25
        $this->displayBlock('prepare_active', $context, $blocks);
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("uts_content_prepare");
        echo "\">Как делать?</a></li>
                        <li ";
        // line 26
        $this->displayBlock('task_active', $context, $blocks);
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("uts_content_task");
        echo "\">Что делать?</a></li>
                        <li ";
        // line 27
        $this->displayBlock('test_active', $context, $blocks);
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("uts_hotel_homepage");
        echo "\">Где проверять?</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </div>
        <div class=\"row\">
            <div class=\"col-md-12\">
                ";
        // line 34
        $this->displayBlock('body', $context, $blocks);
        // line 37
        echo "            </div>
        </div>
    </div>
</body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Тестовое задание";
    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 7
        echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/utscontent/css/bootstrap.min.css"), "html", null, true);
        echo "\">
    ";
    }

    // line 9
    public function block_javascripts($context, array $blocks = array())
    {
        // line 10
        echo "        <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/utscontent/js/jquery-2.1.1.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/utscontent/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    // line 24
    public function block_homepage_active($context, array $blocks = array())
    {
    }

    // line 25
    public function block_prepare_active($context, array $blocks = array())
    {
    }

    // line 26
    public function block_task_active($context, array $blocks = array())
    {
    }

    // line 27
    public function block_test_active($context, array $blocks = array())
    {
    }

    // line 34
    public function block_body($context, array $blocks = array())
    {
        // line 35
        echo "
                ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 35,  148 => 34,  143 => 27,  138 => 26,  133 => 25,  128 => 24,  122 => 11,  117 => 10,  114 => 9,  107 => 7,  104 => 6,  98 => 5,  89 => 37,  75 => 27,  69 => 26,  63 => 25,  57 => 24,  42 => 13,  39 => 9,  37 => 6,  33 => 5,  27 => 1,  87 => 34,  74 => 34,  61 => 24,  47 => 13,  38 => 6,  35 => 5,  29 => 3,);
    }
}
