<?php

/* UtsHotelBundle:Default:index.html.twig */
class __TwigTemplate_5bd7feabb81ae4727d263834fc789438d57b34cb11911a7688c748b6978f8dc8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'test_active' => array($this, 'block_test_active'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 5
        $this->env->getExtension('form')->renderer->setTheme((isset($context["searchForm"]) ? $context["searchForm"] : $this->getContext($context, "searchForm")), array(0 => "UtsHotelBundle:Form:search.html.twig"));
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_test_active($context, array $blocks = array())
    {
        echo "class=\"active\"";
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"jumbotron\">
        <h1>Поиск отелей</h1>
        ";
        // line 10
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["searchForm"]) ? $context["searchForm"] : $this->getContext($context, "searchForm")), 'form');
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "UtsHotelBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 10,  40 => 8,  37 => 7,  31 => 3,  26 => 5,);
    }
}
