<?php

/* UtsContentBundle:Default:index.html.twig */
class __TwigTemplate_6a46c532885236702110369ad804c542f8e94a4d9e15774ab3dcb9481cf9a5be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'homepage_active' => array($this, 'block_homepage_active'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_homepage_active($context, array $blocks = array())
    {
        echo "class=\"active\"";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "    <div class=\"jumbotron\">
        <h1>Желаем удачи!</h1>
        <p>
            Этот небольшой проект специально подготовлен для обеспечения удобства в процессе выполнения тестового задания
            и контроля результата.
        </p>
        <p>
            <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("uts_content_prepare");
        echo "\">Приступить</a>
        </p>
    </div>
    <div class=\"row\">
        <div class=\"col-lg-4\">
            <h2>Как делать?</h2>
            <p>
                Ознакомьтесь с тем как подготовить среду для разработки и тестирования. Узнайте каковы требования
                к срокам выполнения заданий и в каком виде нужно предоставить результат на проверку.
            </p>
            <p>
                <a class=\"btn btn-primary btn-mimi\" href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("uts_content_prepare");
        echo "\">Читать >></a>
            </p>
        </div>
        <div class=\"col-lg-4\">
            <h2>Что делать?</h2>
            <p>
                На этой странице подробно изложены условия задач, которые Вам необходимо будет решить.
                Читайте внимательно - внимательность одно из важных условий прохождения теста.
            </p>
            <p>
                <a class=\"btn btn-primary btn-mimi\" href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("uts_content_task");
        echo "\">Читать >></a>
            </p>
        </div>
        <div class=\"col-lg-4\">
            <h2>Где проверять?</h2>
            <p>
                На этой странице реализован простенький, но работоспособный поиск отелей.
                Вы его сделаете лучше и результат ваших трудов найдет здесь своё отражение.
            </p>
            <p>
                <a class=\"btn btn-primary btn-mimi\" href=\"";
        // line 44
        echo $this->env->getExtension('routing')->getPath("uts_hotel_homepage");
        echo "\">Смотреть >></a>
            </p>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-lg-12\"></div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "UtsContentBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 44,  74 => 34,  61 => 24,  47 => 13,  38 => 6,  35 => 5,  29 => 3,);
    }
}
