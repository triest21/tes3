<?php

/* ShtumiUsefulBundle::fields.html.twig */
class __TwigTemplate_d75c54843cda5e696bf9b5bf515ee97d02a3742f337b72cac072d308903e8fb8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'shtumi_ajax_autocomplete_widget' => array($this, 'block_shtumi_ajax_autocomplete_widget'),
            'shtumi_dependent_filtered_entity_widget' => array($this, 'block_shtumi_dependent_filtered_entity_widget'),
            'shtumi_dependent_filtered_select2_widget' => array($this, 'block_shtumi_dependent_filtered_select2_widget'),
            'shtumi_daterange_widget' => array($this, 'block_shtumi_daterange_widget'),
            'shtumi_ajaxfile_widget' => array($this, 'block_shtumi_ajaxfile_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('shtumi_ajax_autocomplete_widget', $context, $blocks);
        // line 67
        echo "

";
        // line 69
        $this->displayBlock('shtumi_dependent_filtered_entity_widget', $context, $blocks);
        // line 112
        echo "
";
        // line 113
        $this->displayBlock('shtumi_dependent_filtered_select2_widget', $context, $blocks);
        // line 147
        echo "

";
        // line 149
        $this->displayBlock('shtumi_daterange_widget', $context, $blocks);
        // line 168
        echo "

";
        // line 170
        $this->displayBlock('shtumi_ajaxfile_widget', $context, $blocks);
        // line 289
        echo "

";
    }

    // line 1
    public function block_shtumi_ajax_autocomplete_widget($context, array $blocks = array())
    {
        // line 2
        echo "
    <script type=\"text/javascript\">
        if (typeof jQuery.ui == 'undefined'){
            jQuery.getScript('";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/shtumiuseful/js/jqueryui/jquery-ui-1.10.1.custom.min.js"), "html", null, true);
        echo "');
        }

    </script>

    <script>
        jQuery(function() {
            \$(\"body\").bind(\"DOMSubtreeModified\", function() {
                jQuery( \"#";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars"), "id"), "html", null, true);
        echo "\"  ).autocomplete({
                    source: function( request, response ) {
                        jQuery.ajax({
                            url: \"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("shtumi_ajaxautocomplete");
        echo "\",
                            dataType: \"json\",
                            data: {
                                maxRows: 12,
                                letters: request.term,
                                entity_alias: \"";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["entity_alias"]) ? $context["entity_alias"] : $this->getContext($context, "entity_alias")), "html", null, true);
        echo "\"
                            },
                            success: function( data ) {
                                response( jQuery.map( data, function( item ) {
                                    return {
                                        label: item,
                                        value: item
                                    }
                                }));
                            }
                        });
                    },
                    minLength: 2,
                    open: function() {
                        jQuery( this ).removeClass( \"ui-corner-all\" ).addClass( \"ui-corner-top\" );
                    },
                    close: function() {
                        jQuery( this ).removeClass( \"ui-corner-top\" ).addClass( \"ui-corner-all\" );
                    }
                });
            });

        });
    </script>

    <style>
        .ui-autocomplete {
            max-height: 100px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }
            /* IE 6 doesn't support max-height
           * we use height instead, but this forces the menu to always be this tall
           */
        * html .ui-autocomplete {
            height: 100px;
        }
    </style>


    ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "

";
    }

    // line 69
    public function block_shtumi_dependent_filtered_entity_widget($context, array $blocks = array())
    {
        // line 70
        echo "
    <select ";
        // line 71
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo "></select>

    <img src='";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/shtumiuseful/images/ajax-loader.gif"), "html", null, true);
        echo "' id='loader' style='display: none;'>
    <script type=\"text/javascript\">
        jQuery(function(){

            jQuery(\"select#";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent"), "offsetGet", array(0 => (isset($context["parent_field"]) ? $context["parent_field"] : $this->getContext($context, "parent_field"))), "method"), "vars"), "id"), "html", null, true);
        echo "\").change( function() {
                var selected_index = ";
        // line 78
        echo twig_escape_filter($this->env, (((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))) ? ((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))) : (0)), "html", null, true);
        echo ";
                jQuery(\"#loader\").show();
                jQuery.ajax({
                    type: \"POST\",
                    data: {
                        parent_id: jQuery(this).val(),
                        entity_alias: \"";
        // line 84
        echo twig_escape_filter($this->env, (isset($context["entity_alias"]) ? $context["entity_alias"] : $this->getContext($context, "entity_alias")), "html", null, true);
        echo "\",
                        empty_value: \"";
        // line 85
        echo twig_escape_filter($this->env, (isset($context["empty_value"]) ? $context["empty_value"] : $this->getContext($context, "empty_value")), "html", null, true);
        echo "\"
                    },
                    url:\"";
        // line 87
        echo $this->env->getExtension('routing')->getPath("shtumi_dependent_filtered_entity");
        echo "\",
                    success: function(msg){
                        if (msg != ''){
                            jQuery(\"select#";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars"), "id"), "html", null, true);
        echo "\").html(msg).show();
                            jQuery.each(jQuery(\"select#";
        // line 91
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars"), "id"), "html", null, true);
        echo " option\"), function (index, option){
                                if (jQuery(option).val() == selected_index)
                                    jQuery(option).prop('selected', true);
                            })
                            jQuery(\"select#";
        // line 95
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars"), "id"), "html", null, true);
        echo "\").trigger('change');
                            jQuery(\"#loader\").hide();
                        } else {
                            jQuery(\"select#";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars"), "id"), "html", null, true);
        echo "\").html('<em>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["no_result_msg"]) ? $context["no_result_msg"] : $this->getContext($context, "no_result_msg"))), "html", null, true);
        echo "</em>');
                            jQuery(\"#loader\").hide();
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError){
                    jQuery('html').html(xhr.responseText);
                    }
                });
            });
            jQuery(\"select#";
        // line 107
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent"), "offsetGet", array(0 => (isset($context["parent_field"]) ? $context["parent_field"] : $this->getContext($context, "parent_field"))), "method"), "vars"), "id"), "html", null, true);
        echo "\").trigger('change');
        });
    </script>

";
    }

    // line 113
    public function block_shtumi_dependent_filtered_select2_widget($context, array $blocks = array())
    {
        // line 114
        echo "
    <input ";
        // line 115
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " />

    <script type=\"text/javascript\">
        jQuery(function(){

            \$(\"#";
        // line 120
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars"), "id"), "html", null, true);
        echo "\").select2({
                placeholder: \"";
        // line 121
        echo twig_escape_filter($this->env, (isset($context["empty_value"]) ? $context["empty_value"] : $this->getContext($context, "empty_value")), "html", null, true);
        echo "\",
                minimumInputLength: 0,
                ajax: {
                    url: '";
        // line 124
        echo $this->env->getExtension('routing')->getPath("shtumi_dependent_filtered_select2");
        echo "',
                    dataType: 'json',
                    data: function (term, page) {
                        return {
                            term: term,
                            parent_id: \$(\"#";
        // line 129
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent"), "offsetGet", array(0 => (isset($context["parent_field"]) ? $context["parent_field"] : $this->getContext($context, "parent_field"))), "method"), "vars"), "id"), "html", null, true);
        echo "\").val(),
                            entity_alias: \"";
        // line 130
        echo twig_escape_filter($this->env, (isset($context["entity_alias"]) ? $context["entity_alias"] : $this->getContext($context, "entity_alias")), "html", null, true);
        echo "\"
                        };
                    },
                    results: function (data, page) {
                        return {results: data};
                    }
                }
            });
            \$(\"#";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars"), "id"), "html", null, true);
        echo "\").select2('data', ";
        echo (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"));
        echo ");

            \$(\"#";
        // line 140
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent"), "offsetGet", array(0 => (isset($context["parent_field"]) ? $context["parent_field"] : $this->getContext($context, "parent_field"))), "method"), "vars"), "id"), "html", null, true);
        echo "\").change(function(){
                \$(\"#";
        // line 141
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars"), "id"), "html", null, true);
        echo "\").select2('data', null);
            });
        });
    </script>

";
    }

    // line 149
    public function block_shtumi_daterange_widget($context, array $blocks = array())
    {
        // line 150
        echo "
    <style type=\"text/css\">@import \"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/shtumiuseful/js/datepicker/jquery.datepick.css"), "html", null, true);
        echo "\";</style>
    <style type=\"text/css\">@import \"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/shtumiuseful/css/daterange.css"), "html", null, true);
        echo "\";</style>
    <script type=\"text/javascript\" src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/shtumiuseful/js/datepicker/jquery.datepick.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("bundles/shtumiuseful/js/datepicker/jquery.datepick-" . twig_slice($this->env, (isset($context["locale"]) ? $context["locale"] : $this->getContext($context, "locale")), 0, 2)) . ".js")), "html", null, true);
        echo "\"></script>

    <input ";
        // line 156
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " value=\"";
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
        echo "\" class=\"shtumi-daterange\">

    <script>
        jQuery('input#";
        // line 159
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars"), "id"), "html", null, true);
        echo "').datepick({
            rangeSelect: true, monthsToShow: 2, showTrigger: '#calImg', dateFormat: '";
        // line 160
        echo twig_escape_filter($this->env, (isset($context["datepicker_date_format"]) ? $context["datepicker_date_format"] : $this->getContext($context, "datepicker_date_format")), "html", null, true);
        echo "' });
    </script>

    <div style=\"display: none;\">
        <img id=\"calImg\" src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/shtumiuseful/js/datepicker/calendar-green.gif"), "html", null, true);
        echo "\" alt=\"Popup\" class=\"trigger\" height='22' align='absmiddle' style='margin-left: 10px;'>
    </div>

";
    }

    // line 170
    public function block_shtumi_ajaxfile_widget($context, array $blocks = array())
    {
        // line 171
        echo "
    <style type=\"text/css\">@import \"";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/shtumiuseful/css/jquery-file-upload/jquery.fileupload.css"), "html", null, true);
        echo "\";</style>

    <script type=\"text/javascript\" src=\"http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js\"></script>
    <script type=\"text/javascript\" src=\"http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/shtumiuseful/js/jquery-file-upload/jquery.iframe-transport.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/shtumiuseful/js/jquery-file-upload/jquery.fileupload.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/shtumiuseful/js/jquery-file-upload/jquery.fileupload-process.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/shtumiuseful/js/jquery-file-upload/jquery.fileupload-audio.js"), "html", null, true);
        echo "\"></script>

    <script type=\"text/javascript\" src=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/shtumiuseful/js/jquery-file-upload/jquery.fileupload-video.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/shtumiuseful/js/jquery-file-upload/jquery.fileupload-validate.js"), "html", null, true);
        echo "\"></script>


    <!-- The fileinput-button span is used to style the file input field as button -->
    <span class=\"btn btn-success fileinput-button\">
        <i class=\"glyphicon glyphicon-plus\"></i>
        <span>Add files...</span>
        <!-- The file input field used as target for the file upload widget -->
        ";
        // line 190
        $context["uploader_id"] = ((isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")) . "_uploader");
        // line 191
        echo "        <input id=\"";
        echo twig_escape_filter($this->env, (isset($context["uploader_id"]) ? $context["uploader_id"] : $this->getContext($context, "uploader_id")), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "\" type=\"file\" multiple>

        <input ";
        // line 193
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " type=\"hidden\">

    </span>


    <div class=\"progress-bar progress-bar-success\"></div>
    <!-- The container for the uploaded files -->
    <div id=\"files\" class=\"files\"></div>

    <script>

        (function(){

            var files_list = [];

            \$('#";
        // line 208
        echo twig_escape_filter($this->env, (isset($context["uploader_id"]) ? $context["uploader_id"] : $this->getContext($context, "uploader_id")), "html", null, true);
        echo "')
                .fileupload({
                    url: '";
        // line 210
        echo $this->env->getExtension('routing')->getPath("shtumi_ajaxfileupload");
        echo "',
                    dataType: 'json',
                    autoUpload: true,
                    maxFileSize: 50000000000, // 5 MB
                    // Enable image resizing, except for Android and Opera,
                    // which actually support image resizing, but fail to
                    // send Blob objects via XHR requests:
                    disableImageResize: false,// /Android(?!.*Chrome)|Opera/
                            //.test(window.navigator.userAgent),
                    previewMaxWidth: 100,
                    previewMaxHeight: 100,
                    previewCrop: true
                })
                .on('fileuploadadd', function (e, data) {
                    data.context = \$('<div/>').appendTo('#files');
                    \$.each(data.files, function (index, file) {
                        var node = \$('<p/>')
                            .append(\$('<span/>').text(file.name));

                        node.appendTo(data.context);
                    });
                })
                .on('fileuploadprocessalways', function (e, data) {
                    var index = data.index,
                        file = data.files[index],
                        node = \$(data.context.children()[index]);
                    if (file.preview) {
                        node
                            .prepend('<br>')
                            .prepend(file.preview);
                    }
                    if (file.error) {
                        node
                            .append('<br>')
                            .append(\$('<span class=\"text-danger\"/>').text(file.error));
                    }
                })
                .on('fileuploadprogressall', function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    \$('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                })
                .on('fileuploaddone', function (e, data) {
                    console.log(data);
                    \$.each(data.result.files, function (index, file) {
                        alert(123);
                        if (file.url) {
                            var link = \$('<a>')
                                .attr('target', '_blank')
                                .prop('href', file.url);
                            \$(data.context.children()[index])
                                .wrap(link);

                            files_list.push(file.path);
                            \$('#";
        // line 266
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "').val(JSON.stringify(files_list));
                        } else if (file.error) {
                            var error = \$('<span class=\"text-danger\"/>').text(file.error);
                            \$(data.context.children()[index])
                                    .append('<br>')
                                    .append(error);
                        }
                    });
                })
                .on('fileuploadfail', function (e, data) {
                    \$.each(data.files, function (index, file) {
                        var error = \$('<span class=\"text-danger\"/>').text('File upload failed.');
                        \$(data.context.children()[index])
                                .append('<br>')
                                .append(error);
                    });
                })
                .prop('disabled', !\$.support.fileInput)
                        .parent().addClass(\$.support.fileInput ? undefined : 'disabled');
        }());
    </script>

";
    }

    public function getTemplateName()
    {
        return "ShtumiUsefulBundle::fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  482 => 266,  423 => 210,  418 => 208,  400 => 193,  392 => 191,  390 => 190,  379 => 182,  375 => 181,  370 => 179,  366 => 178,  362 => 177,  358 => 176,  351 => 172,  348 => 171,  345 => 170,  337 => 164,  330 => 160,  326 => 159,  318 => 156,  313 => 154,  309 => 153,  305 => 152,  301 => 151,  298 => 150,  295 => 149,  285 => 141,  281 => 140,  274 => 138,  263 => 130,  259 => 129,  251 => 124,  245 => 121,  241 => 120,  233 => 115,  230 => 114,  227 => 113,  218 => 107,  204 => 98,  198 => 95,  191 => 91,  187 => 90,  181 => 87,  176 => 85,  172 => 84,  163 => 78,  159 => 77,  152 => 73,  147 => 71,  144 => 70,  141 => 69,  134 => 64,  88 => 21,  74 => 13,  58 => 2,  55 => 1,  47 => 170,  43 => 168,  41 => 149,  35 => 113,  32 => 112,  30 => 69,  24 => 1,  137 => 38,  131 => 35,  125 => 32,  121 => 31,  115 => 30,  110 => 28,  106 => 27,  100 => 26,  95 => 24,  91 => 23,  85 => 22,  80 => 16,  76 => 19,  70 => 18,  67 => 17,  63 => 5,  53 => 11,  49 => 289,  45 => 8,  42 => 7,  36 => 5,  20 => 1,  44 => 10,  40 => 6,  37 => 147,  31 => 3,  26 => 67,);
    }
}
