<?php

/* UtsContentBundle:Default:prepare.html.twig */
class __TwigTemplate_1d180fde310c9182dc2c021e0f8c73336b9a185793be13a41a8d05b354aa72e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'prepare_active' => array($this, 'block_prepare_active'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_prepare_active($context, array $blocks = array())
    {
        echo "class=\"active\"";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "    <div class=\"row\">
        <div class=\"col-lg-6\">
            <h2>Выполнение</h2>
            <p>
                В процессе выполнения заданий вы можете изменять абсолютно все, что посчитаете нужным.
            </p>
            <p>
                Использование ORM Doctrine предполагает, что проверяющий, получив
                ваш код выполнит команду <span class=\"text-danger\">doctrine:schema:update</span> и применит все
                внесенные вами в структуру БД изменения. Однако, если по каким-то
                причинам этого недостаточно, либо вы считаете необходимым добавление
                каких-то особенных записей в базу, то необходимо добавить в корень
                проекта файл update.sql со всеми необходимыми запросами.
            </p>
            <p>
                Если вы считаете необходимым прокомментровать вашу работу словами,
                то добавьте в корень проекта файл comments.txt со всеми своими
                комментариями.
            </p>
            <p>
                Задания находятся на странице <a href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("uts_content_task");
        echo "\">\"Что делать?\"</a>
            </p>
        </div>
        <div class=\"col-lg-6\">
            <h2>Результаты</h2>
            <p>
                После выполнения каждого их заданий важно зафиксировать изменения.
                Необходимо выполнить команду <span class=\"text-danger\">git add .</span>
                и команду <span class=\"text-danger\">git commit <название задания></span>
            </p>
            <p>
                Создайте аккаунт на bitbucket\\github и репозиторий
            </p>
            <p>
                Загрузите изменения в свой репозиторий, и отправьте ссылку на
            </p>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "UtsContentBundle:Default:prepare.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 26,  38 => 6,  35 => 5,  29 => 3,);
    }
}
