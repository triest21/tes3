<?php

/* UtsHotelBundle:Default:results.html.twig */
class __TwigTemplate_07add7bfcee7a9904bc902aa953e484f24b5dc2c46bcb20b539189a7d6cbbd90 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("UtsHotelBundle:Default:index.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "UtsHotelBundle:Default:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/css/datepicker2.css"), "html", null, true);
        echo "\" />
    <style>

        .alternative_cls{
            background:#b7ebfa;
        }
         </style>
    <div class=\"jumbotron\">
        <h1>Поиск отелей1</h1>
        ";
        // line 14
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["searchForm"]) ? $context["searchForm"] : $this->getContext($context, "searchForm")), 'form');
        echo "
    </div>
    <div class=\"row\">
        <div class=\"col-lg-12\"><h1>";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["request"]) ? $context["request"] : $this->getContext($context, "request")), "city"), "name"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : $this->getContext($context, "request")), "checkIn"), "d.m.Y"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : $this->getContext($context, "request")), "checkOut"), "d.m.Y"), "html", null, true);
        echo "</h1></div>
    </div>
    ";
        // line 19
        if ($this->getAttribute((isset($context["request"]) ? $context["request"] : $this->getContext($context, "request")), "error")) {
            // line 20
            echo "        <div class=\"row\">
            <div class=\"col-lg-12 alert alert-danger\">
                Поиск завершился ошибкой, попробуйте повторить или изменить параметры
            </div>
        </div>
    ";
        } elseif ($this->getAttribute((isset($context["request"]) ? $context["request"] : $this->getContext($context, "request")), "new")) {
            // line 26
            echo "        <div class=\"row\">
            <div class=\"col-lg-12 alert alert-warning\">
                Поиск ещё не был завершен. Повторите попытку позже.
            </div>
        </div>
    ";
        } else {
            // line 32
            echo "        ";
            // line 33
            echo "        <div class=\"row\">
            <div class=\"col-lg-12 text-muted\">
                Всего предложений: ";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "getTotalItemCount"), "html", null, true);
            echo "
            </div>
        </div>
        <div class=\"center-block\">
            ";
            // line 39
            echo $this->env->getExtension('knp_pagination')->render((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
            echo "
        </div>
        ";
            // line 42
            echo "        ";
            $context["date"] = null;
            // line 43
            echo "        ";
            $context["otelindex"] = 0;
            // line 44
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "items"));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                echo "   <!-- Вывод результатов по отелям -->
            ";
                // line 45
                if (((isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")) != $this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "hotel"), "name"))) {
                    // line 46
                    echo "                ";
                    $context["date"] = $this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "hotel"), "name");
                    // line 47
                    echo "                ";
                    $context["otelindex"] = ((isset($context["otelindex"]) ? $context["otelindex"] : $this->getContext($context, "otelindex")) + 1);
                    // line 48
                    echo "                <div class=\"alternative_cls\">
                <div class=\"row\" id=\"hotName\" bgcolor=\"#FF0000\">
                <div class=\"col-lg-6\">
                    <h4>";
                    // line 51
                    echo twig_escape_filter($this->env, (isset($context["otelindex"]) ? $context["otelindex"] : $this->getContext($context, "otelindex")), "html", null, true);
                    echo ". ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "hotel"), "name"), "html", null, true);
                    echo "</h4>
                </div>
                    <div class=\"col-lg-2\">
                    <h4>  ";
                    // line 54
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "mealName"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "price"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "currency"), "html", null, true);
                    echo "</h4>
                    </div>
                </div>
                </div>
            ";
                }
                // line 59
                echo "            <div class=\"row\">

                <div class=\"col-lg-6\">

                    <span class=\"text-primary\">";
                // line 63
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "roomName"), "html", null, true);
                echo "</span>
                    ";
                // line 64
                if ($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "mealName")) {
                    // line 65
                    echo "                        <span class=\"text-muted\">(";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "mealName"), "html", null, true);
                    echo ")</span>
                    ";
                }
                // line 67
                echo "                </div>
                <div class=\"col-lg-2\">";
                // line 68
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "price"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "currency"), "html", null, true);
                echo "</div>
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 71
            echo "        ";
            // line 72
            echo "        <div class=\"center-block\">
            ";
            // line 73
            echo $this->env->getExtension('knp_pagination')->render((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
            echo "
        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "UtsHotelBundle:Default:results.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 73,  176 => 72,  174 => 71,  163 => 68,  160 => 67,  154 => 65,  152 => 64,  148 => 63,  142 => 59,  131 => 54,  123 => 51,  118 => 48,  115 => 47,  112 => 46,  110 => 45,  103 => 44,  100 => 43,  97 => 42,  92 => 39,  85 => 35,  81 => 33,  79 => 32,  71 => 26,  63 => 20,  61 => 19,  52 => 17,  46 => 14,  34 => 5,  31 => 4,  28 => 3,);
    }
}
