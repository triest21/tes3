<?php

/* UtsHotelBundle:SpecialOffer:index.html.twig */
class __TwigTemplate_588c5a9dfc5fa3396079bd3e4885d77bb2f18a1680a57ab58a8758887aaa6b7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Специальные предложения</h1>
    <div class=\"panel panel-heading\">
        <div class=\"col-lg-1\">Id</div>
        <div class=\"col-lg-6\">Название</div>
        <div class=\"col-lg-2\">Скидка</div>
        <div class=\"col-lg-1\">Активно</div>
    </div>
    ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 12
            echo "        <div class=\"row\">
            <div class=\"col-lg-1\"><a href=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("specialoffer_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "</a></div>
            <div class=\"col-lg-6\">";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "name"), "html", null, true);
            echo "</div>
            <div class=\"col-lg-2\">
                ";
            // line 16
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "discountType") == "m")) {
                // line 17
                echo "                    ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "discountValue"), "html", null, true);
                echo " %
                ";
            } else {
                // line 19
                echo "                    ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "discountValue"), "html", null, true);
                echo " руб
                ";
            }
            // line 21
            echo "            </div>
            <div class=\"col-lg-1\">";
            // line 22
            if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "isActive")) {
                echo "Да";
            } else {
                echo "Нет";
            }
            echo "</div>
            <div class=\"col-lg-1\"><a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("specialoffer_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\">edit</a></div>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "    <hr/>
    <div class=\"row\">
        <ul>
            <li>
                <a href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("specialoffer_new");
        echo "\">
                    Добавить предложение
                </a>
            </li>
        </ul>
    </div>

    ";
    }

    public function getTemplateName()
    {
        return "UtsHotelBundle:SpecialOffer:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 30,  92 => 26,  83 => 23,  75 => 22,  72 => 21,  66 => 19,  60 => 17,  58 => 16,  53 => 14,  47 => 13,  44 => 12,  40 => 11,  31 => 4,  28 => 3,);
    }
}
